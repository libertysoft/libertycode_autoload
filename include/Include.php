<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/PatternDecorationRequireInvalidFormatException.php');
include($strRootPath . '/src/config/exception/PatternDecorationInvalidFormatException.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/rule/library/ConstRule.php');
include($strRootPath . '/src/rule/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/rule/exception/RuleCollectionInvalidFormatException.php');
include($strRootPath . '/src/rule/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/rule/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/rule/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/rule/api/RuleInterface.php');
include($strRootPath . '/src/rule/api/RuleCollectionInterface.php');
include($strRootPath . '/src/rule/model/DefaultRule.php');
include($strRootPath . '/src/rule/model/DefaultRuleCollection.php');

include($strRootPath . '/src/rule/pattern/library/ConstPatternRule.php');
include($strRootPath . '/src/rule/pattern/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/rule/pattern/model/PatternRule.php');

include($strRootPath . '/src/rule/factory/library/ConstRuleFactory.php');
include($strRootPath . '/src/rule/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/rule/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/rule/factory/api/RuleFactoryInterface.php');
include($strRootPath . '/src/rule/factory/model/DefaultRuleFactory.php');

include($strRootPath . '/src/rule/factory/standard/library/ConstStandardRuleFactory.php');
include($strRootPath . '/src/rule/factory/standard/model/StandardRuleFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/load/library/ConstLoader.php');
include($strRootPath . '/src/load/exception/RuleCollectionInvalidFormatException.php');
include($strRootPath . '/src/load/model/Loader.php');