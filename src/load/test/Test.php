<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test: Get rule collection hydrated
ob_start();
require_once($strRootAppPath . '/src/build/test/Test.php');
ob_clean();

use liberty_code\autoload\rule\api\RuleInterface;
use liberty_code\autoload\load\model\Loader;



// Autoload registering
require_once($strRootAppPath . '/include/Boot.php');



// Init var
/** @var Loader $objLoader */
$objLoader = Loader::instanceGetDefault();



// Test rule collection
echo('Test rule collection: <br />');

foreach($objLoader->getObjRuleCollection() as $strKey => $objRule)
{
    /** @var RuleInterface $objRule */
	try{
		echo('Rule "' . $strKey . '":');echo('<br />');
		echo('Get: key: <pre>');print_r($objRule->getStrKey());echo('</pre>');
		echo('Get: config: <pre>');print_r($objRule->getTabConfig());echo('</pre>');
		//echo('Get: instance: <pre>');print_r($objDep->getObjInstance());echo('</pre>');
		echo('Get: rule collection count: <pre>');print_r(count($objRule->getObjRuleCollection()->getTabKey()));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');



// Test autoload registering
echo('Test autoload register: <br />');

echo('Check registered: <pre>');var_dump($objLoader->registerCheckIsSet());echo('</pre>');
echo('<pre>');var_dump(spl_autoload_functions());echo('</pre>');
echo('<br />');

echo('Un-register: <pre>');var_dump($objLoader->registerRemove());echo('</pre>');
echo('Un-register: <pre>');var_dump($objLoader->registerRemove());echo('</pre>');
echo('Autoload functions: <pre>');var_dump(spl_autoload_functions());echo('</pre>');
echo('<br />');

echo('Register: <pre>');var_dump($objLoader->registerSet());echo('</pre>');
echo('Register: <pre>');var_dump($objLoader->registerSet());echo('</pre>');
echo('Autoload functions: <pre>');var_dump(spl_autoload_functions());echo('</pre>');

echo('Register force: <pre>');var_dump($objLoader->registerSet(true));echo('</pre>');
echo('Autoload functions: <pre>');var_dump(spl_autoload_functions());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test autoload
echo('Test autoload: <br />');
//$objLoader->registerRemove();

// Test check/get from source
$tabSrc = array(
	'\test_1\test_1_1\ClassTest1', // Found
	'test_1\test_1_2\ClassTest2', // Found
    'test_2\test_2_1\ClassTest3', // Found
    '\test_2\test_2_1\ClassTest4', // Found
	//'\test_3\ClassTest5', // Not found
	//'test_3\ClassTest5', // Not found
	//'test', // Not found
	//3, // Not found
	//6, // Not found
);

foreach($tabSrc as $strSrc)
{
	echo('Test check, get source "'.$strSrc.'": <br />');
	try{
		/** @var test_1\test_1_1\ClassTest1 $objInstance */
		$objInstance = new $strSrc();
		
		echo('Get object: <pre>');print_r($objInstance);echo('</pre>');
		echo('Get value: <pre>');var_dump($objInstance->getStrArg());echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


