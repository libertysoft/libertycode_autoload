<?php

namespace test_1\test_1_2;

use test_1\test_1_1\ClassTest1;


class ClassTest2 extends ClassTest1
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->strArg = 'Class 2';
    }
}