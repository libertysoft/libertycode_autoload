<?php
/**
 * Description :
 * This class allows to provide configuration for default rule tools (model and collection).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\config\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\autoload\config\library\ConstConfig;
use liberty_code\autoload\config\exception\PatternDecorationRequireInvalidFormatException;
use liberty_code\autoload\config\exception\PatternDecorationInvalidFormatException;



/**
 * @method null|string getStrPatternDecoration() Get default decoration for pattern rules, requiring decoration.
 * @method void setBoolPatternDecorationRequire(bool $boolPatternDecorationRequire) Set pattern decoration require option. This option allows to specify if by default a pattern rule need to be decorated.
 * @method void setStrPatternDecoration(string $strPatternDecoration) Set default decoration for pattern rules, requiring decoration.
 */
class DefaultConfig extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE, ConstConfig::DATA_DEFAULT_VALUE_PATTERN_DECORATION_REQUIRED);
		}
		
		if(!$this->beanExists(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION))
		{
			$this->beanAdd(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION, ConstConfig::DATA_DEFAULT_VALUE_PATTERN_DECORATION);
		}
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE,
			ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE:
					PatternDecorationRequireInvalidFormatException::setCheck($value);
					break;

				case ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION:
					PatternDecorationInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check if pattern decoration is required.
	 * This option allows to specify if by default a pattern rule need to be decorated.
     *
     * @return boolean
     */
    public function checkPatternDecorationIsRequired()
    {
        // Return result
        return parent::beanGet(ConstConfig::DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE);
    }
	
	
	
}