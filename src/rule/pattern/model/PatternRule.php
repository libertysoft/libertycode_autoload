<?php
/**
 * Description :
 * This class allows to define pattern rule.
 * Pattern rule allows to get file path from the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     key(optional: got parent key if not found): "pattern rule key",
 *
 *     pattern_match(required): "string REGEXP pattern source (applied to check rule source)",
 *
 *     pattern_replace(optional: got source if not found):
 *         "string REGEXP pattern,
 *         to get element for file path customization",
 *
 *     pattern_file_path(required): "string REGEXP replacement pattern, for file path",
 *
 *     option(optional): [
 *         decoration: true / false
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\pattern\model;

use liberty_code\autoload\rule\model\DefaultRule;

use liberty_code\autoload\rule\library\ConstRule;
use liberty_code\autoload\rule\pattern\library\ConstPatternRule;
use liberty_code\autoload\rule\pattern\exception\ConfigInvalidFormatException;



class PatternRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRule::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods check
	// ******************************************************************************

    /**
     * Check if decoration required.
     *
     * @return boolean
     */
    protected function checkDecorationRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getObjConfig()->checkPatternDecorationIsRequired();

        // Check decoration required
        if(isset($tabConfig[ConstPatternRule::TAB_CONFIG_KEY_OPTION][ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION]))
        {
            $result = (intval($tabConfig[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkMatches($strSrc)
    {
        // Init var
        $result = (preg_match($this->getStrPatternMatch(), $strSrc) == 1);

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get string pattern with decoration if required.
     *
     * @param string $strPattern
     * @return string
     */
    protected function getStrPatternDecoration($strPattern)
    {
        // Init var
        $result = $strPattern;
        $strPatternDecoration = $this->getObjConfig()->getStrPatternDecoration();

        // Set decoration if required
        if($this->checkDecorationRequired() && (!is_null($strPatternDecoration)))
        {
            $result = sprintf($strPatternDecoration, $strPattern);
        }

        // Return result
        return $result;
    }



    /**
     * Get string source match pattern.
     *
     * @return string
     */
    protected function getStrPatternMatch()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getStrPatternDecoration($tabConfig[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH]);

        // Return result
        return $result;
    }



    /**
     * Get string source replace pattern.
     *
     * @return string
     */
    protected function getStrPatternReplace()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getStrPatternMatch();

        // Get pattern replace, if found in config
        if(array_key_exists(ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE, $tabConfig))
        {
            $result = $this->getStrPatternDecoration($tabConfig[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE]);
        }

        // Return result
        return $result;
    }



    /**
     * Get string file path replacement pattern.
     *
     * @return string
     */
    protected function getStrPatternFilePath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_FILE_PATH];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $result = parent::getStrKey();
        $tabConfig = $this->getTabConfig();

        // Get key, if required
        if(array_key_exists(ConstPatternRule::TAB_CONFIG_KEY_KEY, $tabConfig))
        {
            $result = $tabConfig[ConstPatternRule::TAB_CONFIG_KEY_KEY];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrFilePath($strSrc)
    {
        // Init var
        $result = null;
        $strFilePath = preg_replace($this->getStrPatternReplace(), $this->getStrPatternFilePath(), $strSrc);

        // Check file path valid
        if(is_string($strFilePath) && (trim($strFilePath) != ''))
        {
            $result = $strFilePath;
        }

        // Return result
        return $result;
    }
	
	
	
}