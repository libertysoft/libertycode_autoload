<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\pattern\library;



class ConstPatternRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
	const TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH = 'pattern_match';
    const TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE = 'pattern_replace';
    const TAB_CONFIG_KEY_PATTERN_FILE_PATH = 'pattern_file_path';
    const TAB_CONFIG_KEY_OPTION = 'option';
    const TAB_CONFIG_KEY_OPTION_DECORATION = 'decoration';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the pattern rule configuration standard.';
}