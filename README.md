LibertyCode_Autoload
====================



Description
-----------

Library contains auto-loading components, 
allows to manage automatically file inclusion, 
using rules system.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/autoload ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/autoload": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Configuration
-------------

#### Main configuration

- Use following class to configure specific elements
    
    ```php
    use liberty_code\autoload\config\model\DefaultConfig;
    DefaultConfig::instanceGetDefault()->get|set...();
    ```

- Elements configurables

    - Regular expression options, used in rules
    - Global information

---



Usage
-----

#### Rule

Rules system allows to retrieve file path from specified source.

_Elements_

- Rule
    
    Allows to design a rule, 
    who is an item containing configuration array:
    - To check matching, from a specified string source.
    - To get file path, from a specified string source.

- PatternRule

    Extends rule features. 
    Uses regular expressions in configuration array, 
    to check matching and get file path, 
    from a specified string source.

- RuleCollection

    Allows to design collection of rules.
    Uses list of rules:
    - To check matching, from a specified string source.
    - To get file path, from a specified string source.

- RuleFactory

    Allows to design a rule factory, 
    to provide new or specified rule instance,
    from specified configuration.
    
- StandardRuleFactory

    Extends rule factory features. 
    Provides rule instance.
    
_Example_

```php
// Get rule factory
use liberty_code\autoload\rule\factory\standard\model\StandardRuleFactory;
$ruleFactory = new StandardRuleFactory();
...
// Get new rule from configuration
$rule = $ruleFactory->getObjRule(array(...));
...
```

#### Builder

Builder allows to hydrate rule collection with rules, 
from a specified data source.

_Elements_

- DefaultBuilder
    
    Uses array of source data to hydrate rule collection.

_Example_

```php
// Get rule collection
use liberty_code\autoload\rule\model\DefaultRuleCollection;
$ruleCollection = new DefaultRuleCollection();
...
// Get rule builder
use liberty_code\autoload\build\model\DefaultBuilder;
$ruleBuilder = new DefaultBuilder($ruleFactory);
...
// Hydrate rule collection
$ruleBuilder->setTabDataSrc(array(...));
$ruleBuilder->hydrateRuleCollection($ruleCollection);
...
foreach($ruleCollection->getTabKey() as $key) {
    echo($ruleCollection->getObjRule($key)->getStrKey() . '<br />');
}
/**
 * Show: 
 * Rule key 1
 * ...
 * Rule key N
 */
...
```

#### Loader

Loader allows to register auto-loading function, 
using rule collection.

_Example_

```php
use liberty_code\autoload\load\model\Loader;
$loader = Loader::instanceGetDefault();
$loader->setObjRuleCollection($ruleCollection);
...
// Autoload registering
$loader->registerSet();
...
```

---


