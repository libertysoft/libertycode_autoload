<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/rule/factory/test/RuleFactoryTest.php');

// Use
use liberty_code\autoload\rule\model\DefaultRuleCollection;
use liberty_code\autoload\rule\pattern\model\PatternRule;



// Init var
$tabConfig = array(
    'multi_match' => 1,
    'file_path_format_pattern' => $strRootAppPath . '%1$s'
);
$objRuleCollection = new DefaultRuleCollection($tabConfig);
$objRuleFactory->setObjRuleCollection($objRuleCollection);



// Test new rule
$tabRuleData = array(
    [
        [
            'type' => 'test',
            'key' => 'rule_1',
            'pattern_match' => '^\\\\?test_1\\\\.*',
            'pattern_replace' => '^\\\\?test_1\\\\(.*)',
            'pattern_file_path' => '/src/rule/test\\\\test_1\\\\${1}.php'//,
            /*
            'option' => [
                'decoration' => true
            ]
            //*/
        ],
        'rule_1_not care'
    ], // Ko: pattern config provide for unknown rule

    [
        [
            'type' => 'pattern',
            'key' => 'rule_1',
            'pattern_match' => '^\\\\?test_1\\\\.*',
            'pattern_replace' => '^\\\\?test_1\\\\(.*)',
            'pattern_file_path' => '/src/rule/test\\\\test_1\\\\${1}.php'//,
            /*
            'option' => [
                'decoration' => true
            ]
            //*/
        ],
        'rule_1_not care'
    ], // Ok

    [
        [
            'pattern_match' => '#^\\\\?test_2\\\\(.*)#',
            'pattern_file_path' => '/src/rule/test/test_2/${1}.php',
            'option' => [
                'decoration' => false
            ]
        ],
        'rule_2',
        new PatternRule()
    ], // Ok

    [
        [
            'pattern_replace' => '\\\\',
            'pattern_file_path' => '/',
            'option' => [
                'decoration' => '5'
            ]
        ],
        '(.*(test_1|test_2)+.*)'
    ] // Ok
);

foreach($tabRuleData as $ruleData)
{
    echo('Test new rule: <br />');
    echo('<pre>');var_dump($ruleData);echo('</pre>');

    try{
        $tabConfig = $ruleData[0];
        $strConfigKey = (isset($ruleData[1]) ? $ruleData[1] : null);
        $objRule = (isset($ruleData[2]) ? $ruleData[2] : null);
        $objRule = $objRuleFactory->getObjRule($tabConfig, $strConfigKey, $objRule);

        echo('Class path: <pre>');var_dump($objRuleFactory->getStrRuleClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objRule))
        {
            echo('Rule class path: <pre>');var_dump(get_class($objRule));echo('</pre>');
            echo('Rule key: <pre>');var_dump($objRule->getStrKey());echo('</pre>');
            echo('Rule config: <pre>');var_dump($objRule->getTabConfig());echo('</pre>');
        }
        else
        {
            echo('Rule not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


