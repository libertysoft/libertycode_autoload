<?php
/**
 * Description :
 * This include allows to boot autoload registering from a specified rule collection.
 * 
 * @var liberty_code\autoload\rule\api\RuleCollectionInterface $objRuleCollection
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */
 
use liberty_code\autoload\load\model\Loader;



// Init var
$objLoader = new Loader($objRuleCollection);

// Autoload registering
$objLoader->registerSet();