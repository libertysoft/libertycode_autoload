<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\load\library;



class ConstLoader
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
	const DATA_KEY_DEFAULT_RULE_COLLECTION = 'objRuleCollection';

    // Exception message constants
    const EXCEPT_MSG_RULE_COLLECTION_INVALID_FORMAT = 'Following rule collection "%1$s" invalid! It must be a collection object.';
}