<?php
/**
 * Description :
 * This class allows to define default rule factory class.
 * Can be consider is base of all rule factory type.
 *
 * Default rule factory uses the following specified configuration, to get and hydrate rule:
 * [
 *     type(optional): "string constant to determine rule type",
 *
 *     ... specific rule configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\autoload\rule\factory\api\RuleFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\autoload\rule\api\RuleInterface;
use liberty_code\autoload\rule\api\RuleCollectionInterface;
use liberty_code\autoload\rule\exception\RuleCollectionInvalidFormatException;
use liberty_code\autoload\rule\factory\library\ConstRuleFactory;
use liberty_code\autoload\rule\factory\exception\FactoryInvalidFormatException;
use liberty_code\autoload\rule\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|RuleCollectionInterface getObjRuleCollection() Get rule collection object.
 * @method void setObjRuleCollection(null|RuleCollectionInterface $objRuleCollection) Set rule collection object.
 * @method null|RuleFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|RuleFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultRuleFactory extends DefaultFactory implements RuleFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RuleCollectionInterface $objRuleCollection = null
     * @param RuleFactoryInterface $objFactory = null
     */
    public function __construct(
        RuleCollectionInterface $objRuleCollection = null,
        RuleFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init rule collection
        $this->setObjRuleCollection($objRuleCollection);

        // Init rule factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRuleFactory::DATA_KEY_DEFAULT_RULE_COLLECTION))
        {
            $this->__beanTabData[ConstRuleFactory::DATA_KEY_DEFAULT_RULE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstRuleFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstRuleFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRuleFactory::DATA_KEY_DEFAULT_RULE_COLLECTION,
            ConstRuleFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRuleFactory::DATA_KEY_DEFAULT_RULE_COLLECTION:
                    RuleCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstRuleFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified rule.
     * Overwrite it to set specific hydration.
     *
     * @param RuleInterface $objRule
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateRule(RuleInterface $objRule, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstRuleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstRuleFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate rule
        $objRuleCollection = $this->getObjRuleCollection();
        if(!is_null($objRuleCollection))
        {
            $objRule->setRuleCollection($objRuleCollection);
        }

        $objRule->setConfig($tabConfigFormat);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified rule object.
     *
     * @param RuleInterface $objRule
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(RuleInterface $objRule, array $tabConfigFormat)
    {
        // Init var
        $strRuleClassPath = $this->getStrRuleClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strRuleClassPath)) &&
            ($strRuleClassPath == get_class($objRule))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstRuleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstRuleFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of rule,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrRuleClassPathFromType($strConfigType);



    /**
     * Get string class path of rule engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrRuleClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrRuleClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRuleClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrRuleClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrRuleClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance rule,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|RuleInterface
     */
    protected function getObjRuleNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrRuleClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance rule engine.
     *
     * @param array $tabConfigFormat
     * @param RuleInterface $objRule = null
     * @return null|RuleInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjRuleEngine(
        array $tabConfigFormat,
        RuleInterface $objRule = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objRule = (
            is_null($objRule) ?
                $this->getObjRuleNew($strConfigType) :
                $objRule
        );

        // Get and hydrate rule, if required
        if(
            (!is_null($objRule)) &&
            $this->checkConfigIsValid($objRule, $tabConfigFormat)
        )
        {
            $this->hydrateRule($objRule, $tabConfigFormat);
            $result = $objRule;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjRule(
        array $tabConfig,
        $strConfigKey = null,
        RuleInterface $objRule = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjRuleEngine($tabConfigFormat, $objRule);

        // Get rule from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjRule($tabConfig, $strConfigKey, $objRule);
        }

        // Return result
        return $result;
    }



}