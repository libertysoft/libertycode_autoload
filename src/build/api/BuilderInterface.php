<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified rule collection instance, with routes.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\build\api;

use liberty_code\autoload\rule\api\RuleCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified rule collection.
     *
     * @param RuleCollectionInterface $objRuleCollection
     * @param boolean $boolClear = true
     */
    public function hydrateRuleCollection(RuleCollectionInterface $objRuleCollection, $boolClear = true);
}