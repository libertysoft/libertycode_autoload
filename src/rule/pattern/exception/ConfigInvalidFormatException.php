<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\pattern\exception;

use liberty_code\autoload\rule\pattern\library\ConstPatternRule;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPatternRule::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstPatternRule::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstPatternRule::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstPatternRule::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid source match pattern
            isset($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH]) &&
            is_string($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH]) &&
            (trim($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH]) != '') &&

            // Check valid source replace pattern
            (
                (!isset($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE])) ||
                (
                    is_string($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE]) &&
                    (trim($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE]) != '') &&
                    (trim($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_REPLACE]) != '')
                )
            ) &&

            // Check valid file path replacement pattern
            isset($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_FILE_PATH]) &&
            is_string($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_FILE_PATH]) &&
            (trim($config[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_FILE_PATH]) != '') &&

            // Check valid option decoration
            (
                (!isset($config[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                    [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                    [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    is_int($config[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                    [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION]) ||
                    (
                        is_string($config[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                        [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION]) &&
                        ctype_digit($config[ConstPatternRule::TAB_CONFIG_KEY_OPTION]
                        [ConstPatternRule::TAB_CONFIG_KEY_OPTION_DECORATION])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}