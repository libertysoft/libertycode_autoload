<?php
/**
 * Description :
 * This class allows to define standard rule factory class.
 * Standard rule factory allows to provide and hydrate rule instance.
 *
 * Standard rule factory uses the following specified configuration, to get and hydrate rule:
 * [
 *     -> Configuration key(optional):
 *         "rule key OR pattern_match (if pattern_match not found on pattern rule configuration)"
 *     type(optional): "pattern",
 *     Pattern rule configuration (@see PatternRule )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\factory\standard\model;

use liberty_code\autoload\rule\factory\model\DefaultRuleFactory;

use liberty_code\autoload\rule\pattern\library\ConstPatternRule;
use liberty_code\autoload\rule\pattern\model\PatternRule;
use liberty_code\autoload\rule\factory\standard\library\ConstStandardRuleFactory;



class StandardRuleFactory extends DefaultRuleFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Use configuration key, if required
        if(!is_null($strConfigKey))
        {
            // Configuration key considered as source, if required
            if(!array_key_exists(ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH, $result))
            {
                $result[ConstPatternRule::TAB_CONFIG_KEY_PATTERN_SOURCE_MATCH] = $strConfigKey;
            }
            // Case else: configuration key considered as rule key, if required
            else if((!array_key_exists(ConstPatternRule::TAB_CONFIG_KEY_KEY, $result)))
            {
                $result[ConstPatternRule::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRuleClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of rule, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardRuleFactory::CONFIG_TYPE_PATTERN:
                $result = PatternRule::class;
                break;
        }

        // Return result
        return $result;
    }



}