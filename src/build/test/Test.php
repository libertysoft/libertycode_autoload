<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/rule/factory/test/RuleFactoryTest.php');

use liberty_code\autoload\rule\model\DefaultRuleCollection;
use liberty_code\autoload\build\model\DefaultBuilder;



// Init var
$tabConfig = array(
    'multi_match' => 1,
    'file_path_format_pattern' => $strRootAppPath . '%1$s'
);
$objRuleCollection = new DefaultRuleCollection($tabConfig);
$objBuilder = new DefaultBuilder($objRuleFactory);

$tabDataSrc = array(
    'rule_1' => [
        'pattern_match' => '^\\\\?test_1\\\\.*',
        'pattern_replace' => '^\\\\?test_1\\\\(.*)',
        'pattern_file_path' => '/src/load/test\\\\test_1\\\\${1}.php',
        'option' => [
            'decoration' => true
        ]
    ],
    '#^\\\\?test_2\\\\(.*)#' => [
        'pattern_file_path' => '/src/load/test/test_2/${1}.php',
        'option' => [
            'decoration' => false
        ]
    ],
    '(.*(test_1|test_2)+.*)' => [
        'pattern_replace' => '\\\\',
        'pattern_file_path' => '/',
        'option' => [
            'decoration' => '5'
        ]
    ] /*,
	'rule_2' => [
        'key1' => 'Value 1',
        'key2' => 'Value 2',
        'option' => [
            'key3' => 'Value 3'
        ]
    ]
	//*/
);



// Test properties
echo('Test properties: <br />');
echo('Data source initialization: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

$objBuilder->setTabDataSrc($tabDataSrc);
echo('Data source hydrated: <pre>');print_r($objBuilder->getTabDataSrc());echo('</pre>');

echo('<br />');

echo('<br /><br /><br />');



// Test hydrate rule collection
echo('Test hydrate rule collection: <br />');

$objBuilder->hydrateRuleCollection($objRuleCollection);
foreach($objRuleCollection->getTabKey() as $strKey)
{
    $objRule = $objRuleCollection->getObjRule($strKey);

	try{
		echo('Rule "' . $strKey . '":');echo('<br />');
		echo('Get: key: <pre>');print_r($objRule->getStrKey());echo('</pre>');
		echo('Get: config: <pre>');print_r($objRule->getTabConfig());echo('</pre>');
		//echo('Get: instance: <pre>');print_r($objDep->getObjInstance());echo('</pre>');
		echo('Get: rule collection count: <pre>');print_r(count($objRule->getObjRuleCollection()->getTabKey()));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	
	echo('<br /><br />');
}

echo('<br /><br /><br />');


