<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

use liberty_code\autoload\rule\api\RuleInterface;
use liberty_code\autoload\rule\model\DefaultRuleCollection;
use liberty_code\autoload\rule\pattern\model\PatternRule;



// Init var
$tabConfig = array(
    'multi_match' => 1,
    'file_path_format_pattern' => $strRootAppPath . '%1$s'
);
$objRuleCollection = new DefaultRuleCollection($tabConfig);



// Test add rule
echo('Test add : <br />');

$objRule1 = new PatternRule(
    array(
        'key' => 'rule_1',
        'pattern_match' => '^\\\\?test_1\\\\.*',
        'pattern_replace' => '^\\\\?test_1\\\\(.*)',
        'pattern_file_path' => '/src/rule/test\\\\test_1\\\\${1}.php'//,
        /*
        'option' => [
            'decoration' => true
        ]
        //*/
    )
);

$objRule2 = new PatternRule(
    array(
        'pattern_match' => '#^\\\\?test_2\\\\(.*)#',
        'pattern_file_path' => '/src/rule/test/test_2/${1}.php',
        'option' => [
            'decoration' => false
        ]
    )
);

$objRule3 = new PatternRule(
    array(
        'pattern_match' => '(.*(test_1|test_2)+.*)',
		'pattern_replace' => '\\\\',
        'pattern_file_path' => '/',
        'option' => [
            'decoration' => '5'
        ]
    )
);

try{
    $objRule4 = new PatternRule(
        array(
            'test' => '(.*(test_1|test_2)+.*)',
            'test_2' => '\\\\',
        )
    );
} catch (\Exception $e) {
    echo(htmlentities(get_class($e) . ' - ' . $e->getMessage()));
    echo('<br />');
}

$objRuleCollection->setRule($objRule1);
$objRuleCollection->setRule($objRule2);
$objRuleCollection->setRule($objRule3);

echo('<pre>');print_r($objRuleCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



// Test check/get from source
$tabSrc = array(
	'\test_1\test_1_1\ClassTest1', // Found
	'test_1\test_1_2\ClassTest2', // Found
    'test_2\test_2_1\ClassTest3', // Found
    '\test_2\test_2_1\ClassTest4', // Found
	'\test_3\ClassTest5', // Ko: not found
	'test_3\ClassTest5', // Ko: not found
	'test', // Ko: not found
	3, // Ko: not found
	6, // Ko: not found
);

foreach($tabSrc as $strSrc)
{
	echo('Test check, get source "'.$strSrc.'": <br />');
	try{
		$tabRule = $objRuleCollection->getTabRule($strSrc);
        $strFilePath = $objRuleCollection->getStrFilePath($strSrc);
		$boolMatch = $objRuleCollection->checkMatches($strSrc);
		
		echo('Check: <pre>');var_dump($boolMatch);echo('</pre>');
		echo('Count: <pre>');var_dump(count($tabRule));echo('</pre>');
        echo('File path: <pre>');var_dump($strFilePath);echo('</pre>');
		
		if(count($tabRule) > 0)
		{
            /** @var RuleInterface $objRule */
			$objRule = $tabRule[0];
			echo('Get: rule key: <pre>');print_r($objRule->getStrKey());echo('</pre>');
            echo('Get: rule match: <pre>');var_dump($objRule->checkMatches($strSrc));echo('</pre>');
            echo('Get: rule file path: <pre>');print_r($objRule->getStrFilePath($strSrc));echo('</pre>');
            echo('Get: collection rule file path: <pre>');print_r($strFilePath);echo('</pre>');
		}
		else
		{
			echo('Get: unfound<br />');
		}
	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


