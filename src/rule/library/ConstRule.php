<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\library;



class ConstRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_RULE_COLLECTION = 'objRuleCollection';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



	// Configuration
    const CONF_DEFAULT_RULE_HASH_PREFIX = 'default_rule_';

    // Collection configuration keys
    const TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH = 'multi_match';
    const TAB_COLLECTION_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN = 'file_path_format_pattern';



    // Exception message constants
    const EXCEPT_MSG_RULE_COLLECTION_INVALID_FORMAT = 'Following rule collection "%1$s" invalid! It must be a rule collection object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default rule configuration standard.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
	const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a rule object in collection.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default rule collection configuration standard.';
}