<?php
/**
 * Description :
 * This class allows to define default rule collection class.
 * key: rule key => rule.
 * Default rule collection uses the following specified configuration:
 * [
 *     multi_match(optional: got false if not found): true / false,
 *
 *     file_path_format_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by file path",
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\autoload\rule\api\RuleCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\autoload\config\model\DefaultConfig;
use liberty_code\autoload\rule\library\ConstRule;
use liberty_code\autoload\rule\api\RuleInterface;
use liberty_code\autoload\rule\exception\CollectionConfigInvalidFormatException;
use liberty_code\autoload\rule\exception\CollectionKeyInvalidFormatException;
use liberty_code\autoload\rule\exception\CollectionValueInvalidFormatException;



class DefaultRuleCollection extends DefaultBean implements RuleCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var array */
    protected $tabConfig;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null, array $tabData = array())
    {
        // Init var
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			// Check value argument
			CollectionValueInvalidFormatException::setCheck($value);

			// Check key argument
			/** @var RuleInterface $value */
			if(
				(!is_string($key)) ||
				($key != $value->getStrKey())
			)
			{
				throw new CollectionKeyInvalidFormatException($key);
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkMultiMatch()
    {
        // Init var
        $result = false;
        $tabConfig = $this->getTabConfig();

        // Get from configuration if found
        if(array_key_exists(ConstRule::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH, $tabConfig))
        {
            $result = (intval($tabConfig[ConstRule::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjRule($strKey)));
    }



	/** 
	 * @inheritdoc
	 */
	public function checkMatches($strSrc)
	{
		// Return result
        return (count($this->getTabRule($strSrc)) > 0);
	}
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get rule configuration.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjRule($strKey)
    {
        // Init var
        $result = null;

        // Get rule if found
        if($this->beanDataExists($strKey))
        {
            $result = $this->beanGetData($strKey);
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getTabRule($strSrc)
	{
		// Init var
		$result = array();
		$boolMultiMatch = $this->checkMultiMatch();
		
		// Run all rules
		foreach($this as $strKey => $objRule)
		{
			/** @var RuleInterface $objRule */
			
			// Check rule found (matches with source)
			if($objRule->checkMatches($strSrc))
			{
				// Set result
				$result[] = $objRule;
				
				// Break loop if multi match not selected
				if(!$boolMultiMatch)
				{
					break;
				}
			}
		}
		
		// Return result
		return $result;
	}



    /**
     * Get formatted file path,
     * from specified file path.
     *
     * @param string $strFilePath
     * @param mixed $default = null
     * @return mixed|string
     */
    protected function getStrFilePathFormat($strFilePath, $default = null)
    {
        // Init var
        $result = $default;
        $tabConfig = $this->getTabConfig();

        // Get format pattern
        $strPattern = null;
        if(array_key_exists(ConstRule::TAB_COLLECTION_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN, $tabConfig))
        {
            $strPattern = $tabConfig[ConstRule::TAB_COLLECTION_CONFIG_KEY_FILE_PATH_FORMAT_PATTERN];
        }

        // Set format, if required
        if(!is_null($strPattern))
        {
            $result = sprintf($strPattern, $strFilePath);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrFilePath($strSrc)
    {
        // Init var
        $result = null;
        $tabRule = $this->getTabRule($strSrc);

		// Run all rules found
		foreach($tabRule as $objRule)
		{
			// Case first time: set result from source
			if(is_null($result))
			{
				$result = $objRule->getStrFilePath($strSrc);
			}
			// Case else: set result from previous result
			else
			{
				$result = $objRule->getStrFilePath($result);
			}
		}

		// Format file path, if required
		if(!is_null($result))
		{
            $result = $this->getStrFilePathFormat($result, $result);
		}

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;
    }



    /**
     * @inheritdoc
     * @throws CollectionConfigInvalidFormatException
     */
    public function setMultiMatch($boolMultiMatch)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set multi-match option
        $tabConfig[ConstRule::TAB_COLLECTION_CONFIG_KEY_MULTI_MATCH] = $boolMultiMatch;

        // Set configuration
        $this->setConfig($tabConfig);
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setRule(RuleInterface $objRule)
	{
		// Init var
		$strKey = $objRule->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objRule);
		
		// return result
		return $strKey;
	}
	
	
	
	/**
     * @inheritdoc
     * @throws CollectionKeyInvalidFormatException
     * @throws CollectionValueInvalidFormatException
     */
    protected function beanSet($key, $val, $boolTranslate = false)
    {
        // Call parent method
        parent::beanSet($key, $val, $boolTranslate);

		// Register dependency collection on dependency object
		/** @var RuleInterface $val */
		$val->setRuleCollection($this);
    }



    /**
     * @inheritdoc
     */
    public function setTabRule($tabRule)
    {
        // Init var
        $result = array();

        // Case index array of rules
        if(is_array($tabRule))
        {
            // Run all rules and for each, try to set
            foreach($tabRule as $rule)
            {
                $strKey = $this->setRule($rule);
                $result[] = $strKey;
            }
        }
        // Case collection of rules
        else if($tabRule instanceof RuleCollectionInterface)
        {
            // Run all routes and for each, try to set
            foreach($tabRule->getTabKey() as $strKey)
            {
                $objRule = $tabRule->getObjRule($strKey);
                $strKey = $this->setRule($objRule);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRule($strKey)
    {
        // Init var
        $result = $this->getObjRule($strKey);

        // Remove route
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRuleAll()
    {
        // Ini var
        $tabKey = $this->getTabKey();

        foreach($tabKey as $strKey)
        {
            $this->removeRule($strKey);
        }
    }



}