<?php
/**
 * Description :
 * This class allows to describe behavior of rules collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\api;

use liberty_code\autoload\rule\api\RuleInterface;



interface RuleCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

    /**
     * Check if multi match option is selected.
     * This option allows to specify if search include all rules matched (false means include only first rule matched).
     *
     * @return boolean
     */
    public function checkMultiMatch();



    /**
     * Check if specified rule key is found.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);



    /**
     * Check if specified source matches with at least one rule.
     *
     * @param string $strSrc
     * @return boolean
     */
    public function checkMatches($strSrc);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get index array of keys.
     *
     * @return array
     */
    public function getTabKey();



    /**
     * Get rule from specified key.
     *
     * @param string $strKey
     * @return null|RuleInterface
     */
    public function getObjRule($strKey);



	/**
	 * Get index array of rules from specified source.
	 * 
	 * @param string $strSrc
	 * @return RuleInterface[]
	 */
	public function getTabRule($strSrc);
	
	
	
	/**
     * Get string file path from specified source.
     *
	 * @param string $strSrc
     * @return null|string
     */
    public function getStrFilePath($strSrc);
	
	

	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set multi match option.
     * This option allows to specify if search include all rules matched (false means include only first rule matched).
     *
     * @param boolean $boolMultiMatch
     */
    public function setMultiMatch($boolMultiMatch);



	/**
	 * Set rule and return its key.
	 * 
	 * @param RuleInterface $objRule
	 * @return string
     */
	public function setRule(RuleInterface $objRule);



    /**
     * Set list of rules (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|RuleCollectionInterface $tabRule
     * @return array
     */
    public function setTabRule($tabRule);



    /**
     * Remove rule and return its instance.
     *
     * @param string $strKey
     * @return RuleInterface
     */
    public function removeRule($strKey);



    /**
     * Remove all rules.
     */
    public function removeRuleAll();
}