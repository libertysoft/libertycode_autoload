<?php
/**
 * Description :
 * This class allows to describe behavior of rule factory class.
 * Rule factory allows to provide new or specified rule instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined rule types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\factory\api;

use liberty_code\autoload\rule\api\RuleInterface;



interface RuleFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of rule,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrRuleClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance rule,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param RuleInterface $objRule = null
     * @return null|RuleInterface
     */
    public function getObjRule(
        array $tabConfig,
        $strConfigKey = null,
        RuleInterface $objRule = null
    );
}