<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\config\library;



class ConstConfig
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PATTERN_DECORATION_REQUIRE = 'boolPatternDecorationRequire';
    const DATA_KEY_DEFAULT_PATTERN_DECORATION = 'strPatternDecoration';

    const DATA_DEFAULT_VALUE_PATTERN_DECORATION_REQUIRED = true;
    const DATA_DEFAULT_VALUE_PATTERN_DECORATION = '#%1$s#';



    // Exception message constants
    const EXCEPT_MSG_PATTERN_DECORATION_REQUIRE_INVALID_FORMAT = 'Following pattern decoration requirement option "%1$s" invalid! The requirement option must be a boolean.';
    const EXCEPT_MSG_PATTERN_DECORATION_INVALID_FORMAT = 'Following pattern decoration "%1$s" invalid! The pattern decoration must be a string, not empty.';


}