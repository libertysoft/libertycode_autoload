<?php

namespace test_2\test_2_1;

use test_1\test_1_1\ClassTest1;



class ClassTest4 extends ClassTest1
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->strArg = 'Class 4';
    }
}