<?php
/**
 * Description :
 * This class allows to define default rule class.
 * Can be consider is base of all rule type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\autoload\rule\api\RuleInterface;

use liberty_code\autoload\config\model\DefaultConfig;
use liberty_code\autoload\rule\library\ConstRule;
use liberty_code\autoload\rule\api\RuleCollectionInterface;
use liberty_code\autoload\rule\exception\ConfigInvalidFormatException;
use liberty_code\autoload\rule\exception\RuleCollectionInvalidFormatException;



abstract class DefaultRule extends FixBean implements RuleInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RuleCollectionInterface $objRuleCollection = null
     */
    public function __construct(array $tabConfig = null, RuleCollectionInterface $objRuleCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init rule collection if required
        if(!is_null($objRuleCollection))
        {
            $this->setRuleCollection($objRuleCollection);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION))
        {
            $this->__beanTabData[ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION] = null;
        }

        if(!$this->beanExists(ConstRule::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRule::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION,
            ConstRule::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION:
                    RuleCollectionInvalidFormatException::setCheck($value);
                    break;

                case ConstRule::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

	/**
     * Get rule configuration.
     *
     * @return DefaultConfig
     */
    public function getObjConfig()
    {
        // Return result
        return DefaultConfig::instanceGetDefault();
    }
	
	
	
	/**
     * @inheritdoc
     */
    public function getObjRuleCollection()
    {
        // Return result
        return $this->beanGet(ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION);
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Return result
        return static::getStrHash($this);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRule::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************
	
	/**
	 * Set dependency collection object.
     *
     * @param RuleCollectionInterface $objRuleCollection = null
	 */
	public function setRuleCollection(RuleCollectionInterface $objRuleCollection = null)
	{
		$this->beanSet(ConstRule::DATA_KEY_DEFAULT_RULE_COLLECTION, $objRuleCollection);
	}



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstRule::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }





    // Methods statics
    // ******************************************************************************

    /**
     * Get string hash
     *
     * @param $objRule
     * @return string
     */
    public static function getStrHash(self $objRule)
    {
        // Return result
        return ConstRule::CONF_DEFAULT_RULE_HASH_PREFIX . spl_object_hash($objRule);
    }



}