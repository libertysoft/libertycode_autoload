<?php
/**
 * Description :
 * This class allows to describe behavior of rule class.
 * Rule is item, containing all information to get file path from specified string source.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\api;

use liberty_code\autoload\rule\api\RuleCollectionInterface;



interface RuleInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods value
	// ******************************************************************************
	
	/**
	 * Check if specified source matches with this rule.
	 * 
	 * @param string $strSrc
	 * @return boolean
	 */
	public function checkMatches($strSrc);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************
	
	/**
	 * Get rule collection object.
	 *
	 * @return null|RuleCollectionInterface
	 */
	public function getObjRuleCollection();
	
	
	
	/**
	 * Get string key (considered as rule id).
	 *
	 * @return string
	 */
	public function getStrKey();


	
    /**
     * Get config array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get file path from specified source.
     *
	 * @param string $strSrc
     * @return null|string
     */
    public function getStrFilePath($strSrc);
	
	
	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set rule collection object.
	 * 
	 * @param RuleCollectionInterface $objRuleCollection = null
	 */
	public function setRuleCollection(RuleCollectionInterface $objRuleCollection = null);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}