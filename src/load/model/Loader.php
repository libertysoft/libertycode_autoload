<?php
/**
 * Description :
 * This class allows to define loader class.
 * Loader allows to register auto-loading function, based on rule collection provided.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\load\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\autoload\rule\api\RuleCollectionInterface;
use liberty_code\autoload\load\library\ConstLoader;
use liberty_code\autoload\load\exception\RuleCollectionInvalidFormatException;



/**
 * @method null|RuleCollectionInterface getObjRuleCollection() Get rule collection object.
 * @method void setObjRuleCollection(null|RuleCollectionInterface $objRuleCollection) Set rule collection object.
 */
class Loader extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;

	


	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * Constructor
	 * 
	 * @param RuleCollectionInterface $objRuleCollection = null
     */
	public function __construct(RuleCollectionInterface $objRuleCollection = null)
	{
		// Call parent constructor
		parent::__construct();
		
		// Hydrate rule collection if needs
		if(!is_null($objRuleCollection))
		{
			$this->setObjRuleCollection($objRuleCollection);
		}
	}



	
	
	// Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstLoader::DATA_KEY_DEFAULT_RULE_COLLECTION))
        {
            $this->beanAdd(ConstLoader::DATA_KEY_DEFAULT_RULE_COLLECTION, null);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstLoader::DATA_KEY_DEFAULT_RULE_COLLECTION
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstLoader::DATA_KEY_DEFAULT_RULE_COLLECTION:
                    RuleCollectionInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * Get file path from specified source.
     *
     * @param string $strSrc
     * @return null|string
     */
    public function getStrFilePath($strSrc)
    {
        // Init var
        $result = null;
        $objRuleCollection = $this->getObjRuleCollection();

        // Check rule collection found
        if(!is_null($objRuleCollection))
        {
            // Get file path
            $result = $objRuleCollection->getStrFilePath($strSrc);
        }

        // Return result
        return $result;
    }





    // Methods register
    // ******************************************************************************

	/**
     * Check if autoloading feature already registered.
     *
     * @return boolean
     */
    public function registerCheckIsSet()
    {
		// Init var
		$result = false;
        $tabAutoload = spl_autoload_functions();
		
		// Run all autoload functions
		$intCpt = 0;
		while(($intCpt < count($tabAutoload)) && (!$result))
		{
			// Get info
			$tabAutoloadInfo = $tabAutoload[$intCpt];
			
			// Check if autoloading feature found
			$result = (
				isset($tabAutoloadInfo[0]) && // Check if potentially object exists
				($tabAutoloadInfo[0] === $this) && // Check if object instance of this class
				isset($tabAutoloadInfo[1]) && // Check if potentially method exists
				is_string($tabAutoloadInfo[1]) && ($tabAutoloadInfo[1] == 'registerRun') // Check if method valid
			);
			
			$intCpt++;
		}
		
		// Return result
        return $result;
    }
	
	
	
    /**
     * Register autoloading feature.
     *
	 * @param boolean $boolForce = false
     * @return boolean
     */
    public function registerSet($boolForce = false)
    {
		// Init var
		$result = false;
		
		// Case force registering: Remove previous function if needs
		if($boolForce)
		{
			$this->registerRemove();
		}
		
		// Set autoloading feature if needs
		if(!$this->registerCheckIsSet())
		{
			$result = (spl_autoload_register(array($this, 'registerRun')) !== false);
		}
		
        // Return result
        return $result;
    }



    /**
     * Unregister autoloading feature.
     *
     * @return boolean
     */
    public function registerRemove()
    {
		// Init var
		$result = false;
		
		// Remove autoloading feature if needs
		if($this->registerCheckIsSet())
		{
			$result = (spl_autoload_unregister(array($this, 'registerRun')) !== false);
		}
		
        // Return result
        return $result;
    }



    /**
     * Include file from specified class path.
     *
     * @param string $strClassPath
     * @return boolean
     */
    public function registerRun($strClassPath)
    {
        // Init var
        $result = false;
        $strFilePath = $this->getStrFilePath($strClassPath);

        // Check file path found and valid
        if(
            (!is_null($strFilePath)) &&
            is_string($strFilePath) &&
            (trim($strFilePath) != '')
        )
        {
            // Include file
            require($strFilePath);
            $result = true;
        }

        // Return result
        return $result;
    }



}