<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\autoload\rule\exception;

use liberty_code\autoload\rule\library\ConstRule;
use liberty_code\autoload\rule\api\RuleCollectionInterface;



class RuleCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $ruleCollection
     */
	public function __construct($ruleCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRule::EXCEPT_MSG_RULE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($ruleCollection), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified rule collection has valid format.
	 * 
     * @param mixed $ruleCollection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($ruleCollection)
    {
		// Init var
		$result = (
			(is_null($ruleCollection)) ||
			($ruleCollection instanceof RuleCollectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($ruleCollection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}